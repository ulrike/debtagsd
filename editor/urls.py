# coding: utf-8
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name="editor_index"),
    url(r'^(?P<name>[^/]+)$', views.Edit.as_view(), name="editor_edit"),
]
