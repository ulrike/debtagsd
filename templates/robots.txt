User-agent: *
Disallow: /accounts/
Disallow: /api/packages/
Disallow: /api/patch/
Disallow: /api/tag/
Disallow: /rep/ # Moved to reports
Disallow: /exports/stable-tags
Disallow: /exports/unstable-tags
Disallow: /exports/vocabulary.js
Disallow: /exports/legacy
