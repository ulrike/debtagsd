from django.conf.urls import url, include
from django.views.generic import TemplateView
import reports.views as rviews
import django.contrib.auth.views as auth_views

urlpatterns = [
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt', content_type="text/plain"), name="root_robots_txt"),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="root_index"),
    url(r'^license/$', TemplateView.as_view(template_name='license.html'), name="root_license"),
    url(r'^doc/sso/$', TemplateView.as_view(template_name='doc/sso.html'), name="root_doc_sso"),
    url(r'^accounts/login/$', auth_views.login, {'template_name': 'registration/login.html'}),
    url(r'^accounts/logout/$', auth_views.logout, {'next_page': '/'}),
    url(r'^api/', include("api.urls")),
    url(r'^rep/', include("reports.urls")),
    url(r'^reports/', include("reports.urls")),
    url(r'^exports/', include("exports.urls")),
    url(r'^edit/', include("editor.urls")),
    url(r'^search/', include("search.urls")),
    url(r'^getting-started/$', TemplateView.as_view(template_name="reports/getting-started.html"), name="getting_started"),
    url(r'^statistics/$', rviews.Stats.as_view(), name="statistics"),
]
