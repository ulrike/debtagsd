from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='reports/index.html'), name="report_index"),
    url(r'^stats/$', views.Stats.as_view(), name="report_stats"),
    url(r'^getting-started/$', TemplateView.as_view(template_name="reports/getting-started.html"), name="report_getting_started"),
    url(r'^todo/$', views.TodoView.as_view(), name="report_todo"),
    url(r'^todo/maint/(?P<email>[^/]+)$', views.TodoMaintView.as_view(), name="report_maint"),
    url(r'^recent/$', views.recent_view, name="report_recent"),
    url(r'^maint/$', TemplateView.as_view(template_name='reports/maint-index.html'), name="report_maint_index"),
    url(r'^maint/(?P<email>[^/]+)$', views.TodoMaintView.as_view(), name="report_todo_maint"),
    url(r'^checks/$', views.list_checks, name="report_checks_list"),
    url(r'^checks/(?P<id>\d+)$', views.show_check, name="report_checks_show"),
    url(r'^pkginfo/(?P<name>[^/]+)$', views.pkginfo_view, name="report_pkginfo"),

    url(r'^facets$', views.facets, name="report_facets"),
    url(r'^facinfo/(?P<name>[^/]+)$', views.facet),  # Compatibility
    url(r'^facets/(?P<name>[^/]+)$', views.facet, name="report_facet"),

    url(r'^taginfo/(?P<name>[^/]+)$', views.taginfo_view, name="report_tag"),
    url(r'^history$', views.AuditLog.as_view(), name="report_audit_log"),
    url(r'^apriori-rules$', views.apriori_rules, name="report_apriori_rules"),

    url(r'^tags_for_maintainer$', views.tags_for_maintainer, name="report_tags_for_maintainer"),
]
